<?php

error_reporting(-1);
ini_set('display_errors', 'On');

use Slim\Http\Request;
use Slim\Http\Response;


// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    $interface = $request->getQueryParam('v');
    foreach ($interface as $key => $iface) {
        $data_array = explode(';', $iface);
        $date = explode('-', $data_array[1]);
        $time = explode(':', $data_array[2]);

        $path = '/data-directory/' . $data_array[0] . '/' . $date[0] . '/' . $date[1] . '/' . $date[2];
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $filename = $time[0] . '.json';
        $path = $path . '/' . $filename;

        if (!file_exists($path)) {
            touch($path);
            chmod($path, 0777);
        }

        clearstatcache();
        $fs = filesize($path);
        $js = [];
        if ($fs > 1) {
            $file = fopen($path, 'r');
            $full_file = fread($file, $fs);
            fclose($file);
            $js = json_decode($full_file, true);

        }
        $file = fopen($path, "w+");
        $js[$time[1]][$time[2]] = [
            'TEMP' => $data_array[3],
            'DEWP' => $data_array[4],
            'STP' => $data_array[5],
            'SLP' => $data_array[6],
            'VISIB' => $data_array[7],
            'WDSP' => $data_array[8],
            'PRCP' => $data_array[9],
            'SNDP' => $data_array[10],
            'FRSHTT' => $data_array[11],
            'CLDC' => $data_array[12],
            'WNDDIR' => $data_array[13]
        ];
        fwrite($file, json_encode($js));
        fclose($file);
    }
    return $response->withStatus(200);
});
